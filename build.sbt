name := "simplex"

version := "0.1"

scalaVersion := "2.13.4"

libraryDependencies  ++= Seq(
  "org.scalanlp" %% "breeze" % "1.1",
  "org.scalanlp" %% "breeze-viz" % "1.1",

  "org.scalaz" %% "scalaz-core" % "7.3.6",
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
)
