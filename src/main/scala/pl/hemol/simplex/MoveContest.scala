package pl.hemol.simplex

case class Contestant3(move: Move, wins: Int, loses: Int) {
  def compete(contestant: Contestant3): Seq[Contestant3] = {
    if (move.beats(contestant.move)) {
      Seq(this.copy(wins = wins + 1), contestant.copy(loses = contestant.loses + 1))
    } else {
      Seq(this.copy(loses = loses + 1), contestant.copy(wins = contestant.wins + 1))
    }
  }
}

object MoveContest {

  def main(args: Array[String]): Unit = {
    val rmg = new RandomMoveGenerator(1)
    var contestants: Seq[Contestant3] = (1 to 10000).map(_ => Contestant3(rmg.nextMove(), 0, 0))
    (1 to 1000).foreach({ _ =>
      contestants = contestants.grouped(2)
        .flatMap(pair => pair(0).compete(pair(1)))
        .toSeq
        .sortBy(_.wins)
    })
    contestants.foreach(println)
  }

}
