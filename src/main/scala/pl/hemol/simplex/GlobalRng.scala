package pl.hemol.simplex

import scala.util.Random

object GlobalRng {

  val rng = new Random(0)

  def nextDouble: Double = rng.nextDouble()

}
