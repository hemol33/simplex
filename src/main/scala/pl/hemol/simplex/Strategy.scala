package pl.hemol.simplex

import java.text.NumberFormat
import scala.util.Random

trait Strategy {

  def nextMove: Move

}

class RmgStrategy(seed: Long = 0L) extends Strategy {
  val rmg = new RandomMoveGenerator(seed)
  override def nextMove: Move = {
    rmg.nextMove()
  }
}

class LinearStrategy(moves: Seq[(Move, Double)], seed: Long = 0L) extends Strategy {
  val rng = new Random(seed)
  override def nextMove: Move = {
    val x: Double = rng.nextDouble()
    var acc: Double = 0
    var candidates: Seq[(Move, Double)] = moves
    var chosen: Move = null
    while (acc <= x) {
      val head = candidates.head
      acc = acc + head._2
      chosen = candidates.head._1
      candidates = candidates.drop(1)
    }
    chosen
  }
  override def toString: String = {
    val formatter = NumberFormat.getNumberInstance
    f"Lin Strategy: ${moves.map(pair => (pair._1, formatter.format(pair._2)))}"
  }
}
