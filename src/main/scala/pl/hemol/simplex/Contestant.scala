package pl.hemol.simplex

trait Contestant[A <: Contestant[A]] {
  def challenge(other: A): Boolean

  def acceptWin(forMove: Move, byMove: Move): Unit
  def acceptLoss(forMove: Move, byMove: Move): Unit

}
