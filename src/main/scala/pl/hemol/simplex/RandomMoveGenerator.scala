package pl.hemol.simplex

import pl.hemol.simplex.RandomMoveGenerator.{heightScaleCoef, sqrt2}

import scala.util.Random

class RandomMoveGenerator(rng: Random) {

  def this(seed: Long = 0L) = {
    this(new Random(seed))
  }

  def nextMove(): Move = {
    val xSquare = rng.nextDouble()
    val ySquare = rng.nextDouble()


    val (x2dTriangle, y2dTriangle) = squareToTriangle(xSquare, ySquare)
    val (x3dTriangle, y3dTriangle, z3dTriangle) = immerseTriangle(x2dTriangle, y2dTriangle)


    Move(x3dTriangle, y3dTriangle, z3dTriangle)
  }

  def nextTrianglePoint(): (Double, Double) = {
    val xSquare = rng.nextDouble()
    val ySquare = rng.nextDouble()


    squareToTriangle(xSquare, ySquare)
  }

  def squareToTriangle(xp: Double, yp: Double): (Double, Double) = {
    var x = xp
    var y = yp
    if (x + y > 1) {
      x = 1 - x
      y = 1 - y
    }
    (
      x + y / 2,
      heightScaleCoef * y
    )
  }

  def immerseTriangle(x: Double, y: Double): (Double, Double, Double) = {
    (
      (x - y / heightScaleCoef / 2),
      (1 - x - y / heightScaleCoef / 2),
      y / heightScaleCoef
    )
  }

}

object RandomMoveGenerator {
  private val heightScaleCoef: Double = Math.sqrt(3) / 2
  private val sqrt2: Double = Math.sqrt(2)
}

object A {
  def main(args: Array[String]): Unit = {
    val rmg = new RandomMoveGenerator(0L)
    val moves = (1 to 10000).map(_ => rmg.nextMove())
    println(moves.map(_.x).sum)
    println(moves.map(_.y).sum)
    println(moves.map(_.z).sum)
    println(moves.map(move => 1 - move.x - move.y - move.z).sum)
    println(moves.map(move => 1 - move.x - move.y - move.z).max)
  }
}
