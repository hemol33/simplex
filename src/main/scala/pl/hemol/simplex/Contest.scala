package pl.hemol.simplex

import scala.collection.mutable
import scala.util.Random

case class ContestantEntry[A <: Contestant[A]](contestant: A, wins: Int, losses: Int) {
  def compete(other: ContestantEntry[A]): Seq[ContestantEntry[A]] = {
    if (contestant.challenge(other.contestant)) {
      Seq(this.copy(wins = wins + 1), other.copy(losses = other.losses + 1))
    } else {
      Seq(this.copy(losses = losses + 1), other.copy(wins = other.wins + 1))
    }
  }
}

object Contest {

  def tournament[A <: Contestant[A]](contestants: Seq[A]): Seq[ContestantEntry[A]] = {
    val entries: mutable.ArrayBuffer[ContestantEntry[A]] = mutable.ArrayBuffer(contestants.map(ContestantEntry(_, 0, 0)): _*)

    (1 to 30).foreach(_ => {
      entries.indices.combinations(2).foreach({ case indices =>
        val results = entries(indices(0)).compete(entries(indices(1)))
        entries(indices(0)) = results(0)
        entries(indices(1)) = results(1)
      })
    })

    entries.toSeq
  }

  def main(args: Array[String]): Unit = {
    val rng = new Random()
    val rmg = new RandomMoveGenerator(rng)
    var strategies: List[LearningStrategy] = (1 to 32).map(_ => {
      val moves = (1 to 10).map(_ => (rmg.nextMove(), 0.1))
      new LearningStrategy(moves, rng)
    }).toList
    var lastTs = System.currentTimeMillis()
    for (i <- 1 to 50000) {
      if (i % 100 == 0) {
        val newTs = System.currentTimeMillis()
        println(s"Generation $i, took: ${newTs - lastTs}")
        lastTs = newTs
      }

      strategies = tournament(strategies).map(_.contestant.nextIteration).toList
    }

    tournament(strategies).sortBy(_.wins).foreach(println)
  }

}
