package pl.hemol.simplex

import scala.collection.mutable
import scala.util.Random

class LearningStrategy(moves: Seq[(Move, Double)], rng: Random) extends LinearStrategy(moves) with Contestant[LearningStrategy] {
  var history: Seq[Move] = Seq()
  var winScore: Map[Move, Int] = moves.map(_._1).map(_ -> 0).toMap
  var beatenMoves: Seq[Move] = Seq()
  var toBeatMoves: Seq[Move] = Seq()

  override def challenge(other: LearningStrategy): Boolean = {
    val myMove = nextMove
    val enemyMove = other.nextMove
    history = enemyMove +: history
    val result = myMove.beats(enemyMove)
    if (result) {
      acceptWin(myMove, enemyMove)
      other.acceptLoss(enemyMove, myMove)
    } else {
      acceptLoss(myMove, enemyMove)
      other.acceptWin(enemyMove, myMove)
    }
    result
  }
  override def acceptWin(forMove: Move, byMove: Move): Unit = {
    beatenMoves = byMove +: beatenMoves
    updateScore(byMove)
  }
  override def acceptLoss(forMove: Move, byMove: Move): Unit = {
    toBeatMoves = byMove +: toBeatMoves
    updateScore(byMove)
  }

  private def updateScore(enemyMove: Move): Unit = {
    winScore = winScore.map({ case (move, score) =>
      move -> (if (move.beats(enemyMove)) score + 1 else score)
    })
  }

  def nextIteration: LearningStrategy = {
    val score = moves.sortBy({ case (move, _) => winScore(move) })

    val changeCount: Int = Math.max(1, moves.size / 3)

    val winners = score.takeRight(changeCount).map(_._1)
    val losers = score.take(changeCount).map(_._1)
    val toChange = (winners ++ losers).toSet
    val toStay = moves.filter(move => !toChange.contains(move._1)).filter(move => move._2 > 0.0001)

    val loserWeight = losers.map(winner => moves.find(_._1.equals(winner)).get).map(_._2).sum
    val newMoves = createNewMoves(changeCount, loserWeight)

    val winnerEntries = winners.map(winner => moves.find(_._1.equals(winner)).get)
      .map(entry => (entry._1, (1.0 + 9 * entry._2) / 10))

    val modifiedWeight = newMoves.map(_._2).sum + winnerEntries.map(_._2).sum
    if (modifiedWeight > 1) {
      val trimmedWinners = winnerEntries.map({ case (move, weight) => (move, weight / modifiedWeight) })
      val trimmedNew = newMoves.map({ case (move, weight) => (move, weight / modifiedWeight) })
      new LearningStrategy(trimmedWinners ++ trimmedNew, rng)
    } else {
      val toStayWeight = toStay.map(_._2).sum
      val remaining = toStay.map({ case (move, weight) => (move, weight * (1 - modifiedWeight) / toStayWeight) })

      new LearningStrategy(winnerEntries ++ remaining ++ newMoves, rng)
    }
  }

  private def createNewMoves(count: Int, weight: Double): Seq[(Move, Double)] = {
    val rmg = new RandomMoveGenerator(rng)

    (1 to count * 10)
      .map(_ => rmg.nextMove())
      .sortBy(move => history.count(move.beats))
      .take(count)
      .map(_ -> weight / count)
  }

  override def toString: String = super.toString
}
