package pl.hemol.simplex

case class Move(x: Double, y: Double, z: Double) {
  def beats(move: Move): Boolean = {
    Seq(x > move.x, y > move.y, z > move.z).count(identity) >= 2
  }

  def +(move: Move): Move = Move(x + move.x, y + move.y, z + move.z)

  def *(scalar: Double): Move = Move(x * scalar, y * scalar, z * scalar)

  def /(scalar: Double): Move = Move(x / scalar, y / scalar, z / scalar)

  override def toString: String = f"Mv($x%.3f, $y%.3f, $z%.3f)"
}
