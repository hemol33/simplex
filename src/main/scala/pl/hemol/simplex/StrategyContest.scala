package pl.hemol.simplex

import scala.util.Random

case class Contestant2(strategy: Strategy, wins: Int, loses: Int) {
  def compete(contestant: Contestant2): Seq[Contestant2] = {
    if (strategy.nextMove.beats(contestant.strategy.nextMove)) {
      Seq(this.copy(wins = wins + 1), contestant.copy(loses = contestant.loses + 1))
    } else {
      Seq(this.copy(loses = loses + 1), contestant.copy(wins = contestant.wins + 1))
    }
  }
}

object StrategyContest {

  def main(args: Array[String]): Unit = {
    val moveSet: Seq[Move] = (0 to 10).flatMap({ i =>
      val x = i.toDouble / 10
      (0 to (10 - i)).map({ j =>
        val y = j.toDouble / 10
        Move(x, y, (10 - i - j ).toDouble / 10)
      })
    })

    val rng = new Random(1L)

    val specialStrategies = List(
      new RmgStrategy,
      new LinearStrategy(List((Move(0.6, 0.3, 0.1), 1))),
      new LinearStrategy(List((Move(0.5, 0.5, 0), 1))),
      new LinearStrategy(List((Move(0.5, 0.5, 0), 0.5), (Move(0.6, 0.3, 0.1), 0.5)))
    )

    val strategies = (1 to 10000).map({ _ =>
      new LinearStrategy({
        (1 to 10).map(_ => (moveSet(rng.nextInt(moveSet.size)), 0.1))
      })
    }) ++ specialStrategies

    var contestants: Seq[Contestant2] = strategies.map(Contestant2(_, 0, 0))

    (1 to 1000).foreach({ _ =>
      contestants = contestants.grouped(2)
        .flatMap(pair => pair(0).compete(pair(1)))
        .toSeq
        .sortBy(_.wins)
    })
    println("Losers")
    contestants.take(3).foreach(println)
    println("Winners")
    contestants.takeRight(3).foreach(println)
    println("Special")
    specialStrategies.foreach(special => contestants.find(_.strategy.equals(special)).foreach(println))
  }

}
