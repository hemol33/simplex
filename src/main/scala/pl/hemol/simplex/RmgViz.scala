package pl.hemol.simplex

import breeze.plot._

object RmgViz {

  def main(args: Array[String]): Unit = {
    val f = Figure()
    val p = f.subplot(0)

    val rmg = new RandomMoveGenerator(0L)

    val points = (1 to 10000).map(_ => rmg.nextMove())
    val xs = points.map(_.x)
    val ys = points.map(_.z)

    p += plot(xs, ys, '+')
  }

}
